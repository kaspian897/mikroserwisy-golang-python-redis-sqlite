package repositories

// var ctx = context.Background()
// var (
// 	Repository IResultRepository
// )

// func init() {
// 	Repository = NewRepository(database.GetInstance())
// }

// type ResultRepository struct {
// 	client *sql.DB
// }
// type IRepository interface {
// 	List(key string, value interface{}) error
// }
// type IResultRepository interface {
// 	IRepository
// }

// func NewRepository(db *sql.DB) *ResultRepository {
// 	return &ResultRepository{
// 		client: db,
// 	}
// }

// func (r *ResultRepository) List(key string, value interface{}) error {
// 	row, err := r.client.Query("SELECT * FROM votes")
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer row.Close()
// 	for row.Next() {
// 		var id int
// 		var name string
// 		var program string
// 		row.Scan(&id, &name, &program)
// 		log.Println("Student: ", " ", name, " ", program)
// 	}
// 	return row.Err()
// }

// import "time"

// type Vote struct {
// 	Title       string    `json:"title"`
// 	Done        bool      `json:"done"`
// 	Description string    `json:"description"`
// 	CreatedAt   time.Time `json:"createdAt"`
// }

// type repository struct {
// 	db []Vote
// }

// var (
// 	r *repository
// )

// // This function will run one time when package is imported before anything else.
// func init() {
// 	r = &repository{
// 		db: []Vote{},
// 	}
// }

// // Repository, Create public interface
// type Repository interface {
// 	ListTodo() []Vote
// 	AddTodo(todo *Vote) Vote
// }

// // Repository, Get instance of singleton repository
// func NewRepository() Repository {
// 	return r
// }

// func (r *repository) ListTodo() []Vote {
// 	return r.db
// }

// func (r *repository) AddTodo(todo *Vote) Vote {
// 	todo.CreatedAt = time.Now()
// 	r.db = append(r.db, *todo)
// 	return *todo
// }
