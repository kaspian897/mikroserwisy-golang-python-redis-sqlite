package config

import (
	"log"
	"os"

	"github.com/joho/godotenv"
)

var (
	REDIS_ADDRESS  string
	REDIS_PASSWORD string
	REDIS_DB       int
	PORT           string
	DB_SQLITE_PATH string
)

func init() {
	err := godotenv.Load()
	if err != nil {
		log.Println("Error loading .env file")
	}
	REDIS_ADDRESS = os.Getenv("REDIS_ADDRESS")
	REDIS_PASSWORD = os.Getenv("REDIS_PASSWORD")
	REDIS_DB = 0
	PORT = os.Getenv("PORT")
	DB_SQLITE_PATH = os.Getenv("DB_SQLITE_PATH")

}
