package models

type Question struct {
	ID      string
	Message string
	Options []Option
}

type Option struct {
	ID       string
	Option   string
	Is_right bool
}
type Answer struct {
	ID       string `gorm:"primaryKey`
	OptionID int
	Option   Option `gorm:"foreignKey:OptionID"`
}
