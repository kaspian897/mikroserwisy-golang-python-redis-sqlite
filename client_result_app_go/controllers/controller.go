package controllers

import (
	"html/template"
	"math"
	"mw/result/database"
	"net/http"
)

// func ParseBody(r *http.Request, x interface{}) error {
// 	body, err := ioutil.ReadAll(r.Body)
// 	if err == nil {
// 		err := json.Unmarshal([]byte(body), x)
// 		if err != nil {
// 			return err
// 		}
// 		return nil
// 	}
// 	return err
// }

// entiteis

type Result struct {
	Count   int
	Name    string
	Percent float64
}

var tpl = template.Must(template.ParseFiles("index.html"))

func ShowResult(w http.ResponseWriter, r *http.Request) {
	// var answers []models.Answer
	var results []Result
	// database.GetInstance().Find(&answers).Select("option, count(*)").Group("option")
	database.GetInstance().Table("answer an").
		Joins("INNER JOIN option op ON op.id = an.option").
		Select("op.option name, count(*) count").
		Group("an.option").
		Scan(&results)

	var sum int
	sum = 0
	for _, r := range results {
		sum += r.Count
	}

	for i := range results {
		var percent = float32(results[i].Count) / float32(sum) * 100
		results[i].Percent = math.Round(float64(percent))
	}
	// fmt.Println("{}", answers)

	// json.NewEncoder(w).Encode(results)
	tpl.Execute(w, results)

}
