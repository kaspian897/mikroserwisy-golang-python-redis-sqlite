from sqlalchemy import Column, Integer, Unicode, UnicodeText, String
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
import os

engine = create_engine("sqlite:////root/db/test.db", echo=True)
Base = declarative_base()

Session = sessionmaker(bind=engine)
#s = Session()
