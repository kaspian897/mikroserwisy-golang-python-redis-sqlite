import redis
import os
from os.path import join, dirname
from dotenv import load_dotenv
from sqlalchemy.sql.expression import true
from models import Answer, Option
import db

import json
from sqlalchemy import exc
from collections import namedtuple


def proceded_check():
    pass

def consumer(r: redis.Redis):
    s = db.Session()
    while True:
        val = r.brpoplpush(src="queue", dst="proceded", timeout=0)
        # print("Consuming: (%s)" % val)
        obj = json.loads(val.decode())
        # print(obj["Option"])
        # answer1 = namedtuple("Answer", obj.keys())(*obj.values())
        # answer = Answer(obj.Option.id)

        answer = Answer(obj["Option"])
        # option = Option(is_right=True,option="aaaaaaaaa")
        # print(obj['Option']+ " aaaaa")
        # print(answer1.Option.id)
        s.add(answer)
        # s.add(option)
        # if(answer.option)
        try:
            s.commit()
            r.lrem("proceded", 1, val)
        except exc.SQLAlchemyError as e:
            print(e)


def main():
    print("Worker: runing")
    dotenv_path = join(dirname(__file__), ".env")
    load_dotenv(dotenv_path)

    REDIS_HOST = os.environ.get("REDIS_HOST")
    REDIS_PORT = os.environ.get("REDIS_PORT")
    REDIS_PASSWORD = os.environ.get("REDIS_PASSWORD")
    REDIS_DB = os.environ.get("REDIS_DB")

    r = redis.Redis(host=REDIS_HOST, port=6379, db=0, password=REDIS_PASSWORD)

    consumer(r)


if __name__ == "__main__":
    main()


def migration():
    db.Base.metadata.create_all()
