from sqlalchemy import Column, Integer, Unicode, UnicodeText, String,ForeignKey
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql.sqltypes import Boolean
from db import Base


#engine = create_engine("sqlite:////tmp/teste.db", echo=True)
#Base = declarative_base(bind=engine)

#Base = declarative_base()
class Option(Base):
    __tablename__ = "option"
    id = Column(Integer, primary_key=True,autoincrement=True)
    option = Column(String(1024))
    is_right = Column(Boolean,default=False)
    def __init__(self, option,is_right):
        self.option = option
        self.is_right=is_right

class Answer(Base):
    __tablename__ = "answer"
    id = Column(Integer, primary_key=True, autoincrement=True)
    option = Column(Integer, ForeignKey('option.id'))

    def __init__(self, option):
        self.option = option

    def __repr__(self):
        return "<Answer(option='%s')>" % (
            self.option,
        )


#Base.metadata.create_all()

#Session = sessionmaker(bind=engine)
#s = Session()
