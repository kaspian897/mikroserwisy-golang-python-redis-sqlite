package database

import (
	"database/sql"
	"fmt"
	"mw/result/config"
	"sync"

	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
	"gorm.io/gorm/schema"
)

var (
	//Db     *sql.DB
	GormDB *gorm.DB
	lock   = &sync.Mutex{}
)

func GetInstance() *gorm.DB {
	// var err error
	// err = nil
	// GormDB, err = gorm.Open(sqlite.Open().(sqlite.Config{
	// 	Conn: Db,
	// }), &gorm.Config{})

	if GormDB == nil {
		lock.Lock()
		defer lock.Unlock()
		if GormDB == nil {
			var err error
			err = nil
			//Db, err = NewDatabase(config.DB_SQLITE_PATH)
			GormDB, err = gorm.Open(sqlite.Open(config.DB_SQLITE_PATH), &gorm.Config{
				NamingStrategy: schema.NamingStrategy{
					SingularTable: true,
				},
			})

			if err != nil {
				fmt.Println(err)
			}
		} else {
		}
	} else {

	}
	return GormDB
}

// type Database struct {
// 	client *sql.DB
// }

// func (db Database) Client() *sql.DB {
// 	return db.client
// }

func NewDatabase(fileName string) (*sql.DB, error) {
	client, err := sql.Open("sqlite3", fileName)
	if err != nil {
		return nil, err
	}
	return client, nil
}
