package main

import (
	"log"
	"mw/result/config"
	"mw/result/controllers"
	"net/http"

	"github.com/gorilla/mux"
)

// var tpl = template.Must(template.ParseFiles("index.html"))

// func indexHandler(w http.ResponseWriter, r *http.Request) {
// 	tpl.Execute(w, nil)
// }
func handleRequests(port string) {

	router := mux.NewRouter().StrictSlash(true)
	router.HandleFunc("/", controllers.ShowResult)
	router.HandleFunc("/answers", controllers.ShowResult)

	fs1 := http.FileServer(http.Dir("static"))
	router.PathPrefix("/").Handler(http.StripPrefix("/static/", fs1))

	// fs2 := http.FileServer(http.Dir("./frontend/dist/"))
	// router.PathPrefix("/").Handler(fs2)

	// fs := http.FileServer(http.Dir("static"))
	// router.PathPrefix("/").Handler(http.StripPrefix("/static/", fs))

	log.Fatal(http.ListenAndServe(":"+port, router))
}
func main() {
	// err := godotenv.Load()
	// if err != nil {
	// 	log.Println("Error loading .env file")
	// }

	// port := os.Getenv("PORT")
	// db, err := database.NewDatabase(os.Getenv("DB_SQLITE_PATH"))
	// repo := repositories.NewRepository(db.Client())
	// svc := services.NewVoteService(repo)
	// for i := 0; i < 100; i++ {
	// 	svc.Push(context.Background(), strconv.Itoa(i))
	// }
	//svc.Push(context.Background(), "aaaa")
	handleRequests(config.PORT)

}
